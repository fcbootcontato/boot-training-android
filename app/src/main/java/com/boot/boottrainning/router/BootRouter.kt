package com.boot.boottrainning.router

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.boot.boottrainning.R
import com.boot.boottrainning.view.splash.SplashActivity

class BootRouter(private val activity: AppCompatActivity) {

    companion object {
        const val CONTRACT_DETAIL_ARG = "CONTRACT_DETAIL_ARG"
    }

    fun goToScreen(tagFirebase: String? = null) {
        addTrackingScreen(tagFirebase)
        activity.startActivity(Intent(activity, SplashActivity::class.java))
    }

    fun goToScreenWithParameters(idContract: String?, tagFirebase: String? = null) {
        addTrackingScreen(tagFirebase)
        Intent(activity, SplashActivity::class.java).apply {
            putExtra(CONTRACT_DETAIL_ARG, idContract)
            activity.startActivity(this)
        }
    }

    fun showActivityClearTop(targetActivity: Class<*>) {
        activity.startActivity(
            Intent(
                activity,
                targetActivity
            ).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
    }

    private fun finishWithAnimation(enterAnim: Int, exitAnim: Int) {
        activity.run {
            finish()
            overridePendingTransition(enterAnim, exitAnim)
        }
    }

    fun finish() {
        finishWithAnimation(R.anim.enter_animation, R.anim.enter_animation)
    }

    fun setResult(resultIntent: Intent) {
        activity.setResult(Activity.RESULT_OK, resultIntent)
    }

    fun onBackPressed() {
//        addTrackingScreen(CreditAnalytics.DEBAUTO_VOLTAR)
        activity.onBackPressed()
    }

    fun finishActivity() {
//        addTrackingScreen(CreditAnalytics.DEBAUTO_FECHAR)
        activity.finish()
    }

    fun finishAffinity() {
        activity.finishAffinity()
    }

    private fun addTrackingScreen(tagFirebase: String?) {
//        tagFirebase?.let { activity.trackerScreenByName(it) }
    }
}