package com.boot.boottrainning.view.base

import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.boot.boottrainning.R
import com.boot.boottrainning.data.Theme
import com.boot.boottrainning.utils.WaveDrawable
import com.boot.boottrainning.view.base.TypeThemes.FORGET_PASSWORD_THEME
import com.boot.boottrainning.view.base.TypeThemes.LOGIN_THEME
import com.boot.boottrainning.view.base.TypeThemes.REGISTER_THEME
import com.boot.boottrainning.view.base.TypeThemes.SUCCESS_THEME
import kotlinx.android.synthetic.main.activity_splash.*


abstract class BaseActivity : AppCompatActivity() {

    var colorBackground: Int? = null
    var colorForeground: Int? = null
    var colorLogoBackground: Int? = null
    var colorStartBackground: Int? = null
    var colorEndBackground: Int? = null
    var colorStatusBar: Int? = null
    var colorNavBar: Int? = null

    /**
     *
     * Simulate user-chosen themes
     * TODO Get from the profile (or online) sharedPreferences
     *
     */
    var listThemes = listOf(
        Theme(
            isGradient = false,
            backgroundColor = R.color.background_1,
            foregroundColor = R.color.foreground_1,
            backgroundLogoColor = R.color.background_logo_1
        ),
        Theme(
            isGradient = false,
            isStatusBarDark = true,
            backgroundColor = R.color.background_2,
            foregroundColor = R.color.foreground_2,
            backgroundLogoColor = R.color.background_logo_2
        ),
        Theme(
            isGradient = false,
            backgroundColor = R.color.background_3,
            foregroundColor = R.color.foreground_3,
            backgroundLogoColor = R.color.background_logo_3
        ),
        Theme(
            isGradient = true,
            backgroundLogoColor = R.color.background_logo_4,
            foregroundColor = R.color.foreground_4,
            startBackgroundColor = R.color.start_color_4,
            endBackgroundColor = R.color.end_color_4
        ),
        LOGIN_THEME,
        FORGET_PASSWORD_THEME,
        REGISTER_THEME,
        SUCCESS_THEME
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    abstract fun init()

    fun setCustomThemeColor(theme: Theme) {
        if (theme.isGradient) {
            theme.startBackgroundColor?.run {
                colorStartBackground = theme.startBackgroundColor
                colorStatusBar = theme.startBackgroundColor
            }
            theme.endBackgroundColor?.run {
                colorEndBackground = theme.endBackgroundColor
                colorNavBar = theme.endBackgroundColor
            }

            if (theme.startBackgroundColor != null && theme.endBackgroundColor != null) {
                val colors = intArrayOf(
                    ContextCompat.getColor(this, theme.startBackgroundColor!!),
                    ContextCompat.getColor(this, theme.endBackgroundColor!!)
                )
                val gd = GradientDrawable(
                    GradientDrawable.Orientation.TOP_BOTTOM, colors
                )
                gd.cornerRadius = 0f
                root.background = gd
            }
        } else {
            theme.backgroundColor?.run {
                colorBackground = theme.backgroundColor

                root.setBackgroundColor(
                    ContextCompat.getColor(this@BaseActivity, this)
                )
                colorStatusBar = this
                colorNavBar = this
            }
        }

        colorStatusBar?.let { it ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.statusBarColor = resources.getColor(it, this.theme)
                window.navigationBarColor = resources.getColor(it, this.theme)
                colorNavBar.let { it2 ->
                    window.navigationBarColor = resources.getColor(it2!!, this.theme)
                }
                if (theme.isStatusBarDark) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    boot_training_name.setTextColor(
                        ContextCompat.getColor(
                            this, R.color.text_semi_dark
                        )
                    )
                }
            } else {
                window.statusBarColor = ContextCompat.getColor(this, it)
                colorNavBar.let { it3 ->
                    window.navigationBarColor = ContextCompat.getColor(this, it3!!)
                }
            }
        }

        colorLogoBackground = theme.backgroundLogoColor
        colorLogoBackground.let {
            val color1: Int = it!!
            val mWaveDrawable =
                WaveDrawable(this, R.drawable.ic_launcher_foreground, color1)

            colorForeground = theme.foregroundColor
            colorForeground.let { it2 ->
                val color2: Int = it2!!

                mWaveDrawable.colorFilter =
                    BlendModeColorFilterCompat
                        .createBlendModeColorFilterCompat(
                            ContextCompat.getColor(
                                this,
                                color2
                            ), BlendModeCompat.SRC_ATOP
                        )

                mWaveDrawable.isIndeterminate = true
                logo.setImageDrawable(mWaveDrawable)
                mWaveDrawable.level = 1273
                mWaveDrawable.setWaveAmplitude(10)
                mWaveDrawable.setWaveLength(250)
                mWaveDrawable.setWaveSpeed(20)
            }
        }
    }
}