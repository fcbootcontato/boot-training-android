package com.boot.boottrainning.view.base

import com.boot.boottrainning.R
import com.boot.boottrainning.data.Theme

/**
 *
 * Themes for the screens according to their type.
 * For example, login screen, registration screen ...
 *
 */
object TypeThemes {
    val LOGIN_THEME = Theme(
        isGradient = true,
        backgroundLogoColor = R.color.background_logo_4,
        foregroundColor = R.color.foreground_4,
        startBackgroundColor = R.color.start_color_4,
        endBackgroundColor = R.color.end_color_4,
        buttonColor = R.color.foreground_4
    )

    val FORGET_PASSWORD_THEME = Theme(
        isGradient = true,
        backgroundLogoColor = R.color.background_logo_5,
        foregroundColor = R.color.foreground_5,
        startBackgroundColor = R.color.start_color_5,
        endBackgroundColor = R.color.end_color_5,
        buttonColor = R.color.foreground_5
    )

    val SUCCESS_THEME = Theme(
        isGradient = true,
        backgroundLogoColor = R.color.background_logo_6,
        foregroundColor = R.color.foreground_6,
        startBackgroundColor = R.color.start_color_6,
        endBackgroundColor = R.color.end_color_6,
        buttonColor = R.color.foreground_6
    )

    val REGISTER_THEME = Theme(
        isGradient = true,
        backgroundLogoColor = R.color.background_logo_7,
        foregroundColor = R.color.foreground_7,
        startBackgroundColor = R.color.start_color_7,
        endBackgroundColor = R.color.end_color_7,
        buttonColor = R.color.foreground_7
    )
}