package com.boot.boottrainning.view.splash

import androidx.lifecycle.ViewModel
import com.boot.boottrainning.router.BootRouter

class SplashViewModel(
    private val router: BootRouter
) : ViewModel() {

    fun onBackButton() {
        router.onBackPressed()
    }

    fun onClickMyContracts() {
//        router.goToMyContracts(CreditAnalytics.TAG)
    }
}