package com.boot.boottrainning.view.splash

import androidx.databinding.DataBindingUtil.setContentView
import com.boot.boottrainning.R
import com.boot.boottrainning.databinding.ActivitySplashBinding
import com.boot.boottrainning.router.BootRouter
import com.boot.boottrainning.view.base.BaseActivity
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class SplashActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashBinding

    private val viewModel: SplashViewModel by viewModel {
        parametersOf(BootRouter(this))
    }

    override fun init() {
        binding = setContentView(
            this,
            R.layout.activity_splash
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        /**
         * To test pre-defined themes, use one of the TypeTheme constants
         */
//        setCustomThemeColor(LOGIN_THEME)
//        setCustomThemeColor(FORGET_PASSWORD_THEME)
//        setCustomThemeColor(REGISTER_THEME)
//        setCustomThemeColor(SUCCESS_THEME)

        /**
         * To simulate a theme chosen by users, use a random
         */
        setCustomThemeColor(listThemes[(listThemes.indices).random()])
    }
}