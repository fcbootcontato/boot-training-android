package com.boot.boottrainning

import com.boot.boottrainning.router.BootRouter
import com.boot.boottrainning.view.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel

import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object AutomaticDebitInjector {
    fun init() = listOf(
        viewModelModule
//        resourceProviderModule,
//        apiModule,
//        repositoryModule,
//        retrofitModule,
//        utilsModule,
//        contractRegisterRequest
    )
}

fun loadBootKoinModules() {
    loadKoinModules(AutomaticDebitInjector.init())
}

//val resourceProviderModule = module {
//    single(override = true) { ResourceProvider(context = get()) }
//}
//
//private val utilsModule = module {
//    factory { FileUtils(context = get()) }
//    factory { DebautoActiveService(context = get()) }
//}
//
//private val apiModule = module {
//    single<ContractRepository> { ContractRepositoryImpl(get()) }
//}

private val viewModelModule = module {
    viewModel { (router: BootRouter) ->
        SplashViewModel(router)
    }
//}
//
//private val repositoryModule = module {
//    single {
//        ConsumerService
//            .getInstance(
//                ContractApi::class.java,
//                get()
//            )
//    }
//}

//private val retrofitModule = module {
//    single(override = true) {
//        EnvironmentManager
//            .instance()
//            .environment
//            .apply { addHeader("X-Brand", "SICREDI") }
//    }
}