package com.boot.boottrainning.data

/**
 * Theme template to customize screens according to what the user prefers
 * or using predefined styles.
 */
data class Theme(
    var id: String? = null,
    var isGradient: Boolean,
    var isStatusBarDark: Boolean = false,
    var backgroundColor: Int? = null,
    var startBackgroundColor: Int? = null,
    var endBackgroundColor: Int? = null,
    var backgroundLogoColor: Int? = null,
    var foregroundColor: Int? = null,
    var buttonColor: Int? = null,
    var disabledButtonColor: Int? = null,
    var editTextColor: Int? = null
)